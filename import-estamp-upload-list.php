<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Stamp</h1>
						<!--<div class="top-right-button-container">
                        <button type="button" class="btn btn-success top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> MANUAL KEY-IN</button>
						<button type="button" class="btn btn-blue top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-upload.png" height="16"></i> UPLOAD  FILE</button>
                    </div>-->
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">file1.csv</li>
								
							</ol>
						</nav>

                    </div>


					<div class="card">
					<div class="card-body p-0">
						<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

                           <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
						   <table  class="data-table data-table-scrollable responsive nowrap skin-tmb">
							
									<thead>
										<tr>
											<th class="text-white">Contract Date</th>
											<th class="text-white">Import Date</th>
											<th class="text-white">Contract No</th>
											<th class="text-white">Tax ID</th>
											<th class="text-white">Customer  </th>
											<th class="text-white">Duty Amount </th>
											<th class="text-white">Charge</th>
											<th class="text-white">Summary</th>
											<th class="text-white text-center">As</th>
											<th class="text-white sort-none text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=10;$i++){ ?>
										<tr>
											<td>9-Jun-2020</td>
											<td>10-Jul-2020</td>
											<td>
												con20200325004<?php echo $i; ?>
											</td>
											<td>456705</td>
											<td>Sompong.A</td>
											<td>1,000.00</td>
											<td>1,000.00</td>
											<td>00.00</td>
											<td>TMB</td>
											<td class=" text-center">
												<a href="#" class="btn  btn-xs p-1 active" title="View"><i class="icon-img"><img src="di/ic-view.png" height="16"></i></a> 												
											</td>
										</tr>
										<?php } ?>
							

									</tbody>
								</table>
								
		

						</div>
						
						<div class="mt-4">
                 
						<div class="d-flex justify-content-end align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-light text-small">Rows per page </span>
									<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
										data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										10
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">5</a>
										<a class="dropdown-item active" href="#">10</a>
										<a class="dropdown-item" href="#">20</a>
									</div>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<span> 1-10 of 40</span>
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<!--<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
									</nav>
								</div>
								
								
							</div>
						
                   		 </div>
						
						
						
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
$(".sub-menu .list-unstyled:nth-child(1)>li").removeClass('active');
$(".sub-menu .list-unstyled:nth-child(1)>li:nth-child(3)").addClass('active');
</script>
</body>

</html>