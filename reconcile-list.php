<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Stamp</h1>
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Reconcile E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Reconcile List</li>
							</ol>
						</nav>

                    </div>

                    <div class="card mb-4">
					<div class="card-body p-3 d-flex justify-content-between align-items-center">

						<h2 class="h5 mb-0 font-weight-bold">EXIM  Source system</h2>
						<a href="reconcile-prepare.php" class="btn btn-primary top-right-button rounded-05"><i class="icon-img"><img src="di/ic-prepair-data.png" height="20"></i> PREPARE  DATA</a>

					</div>
				</div>
					
	

					
					<!--<div id="ds-default">
						<div class="display-default d-flex flex-wrap justify-content-center align-items-center p-5">
							<i class="icon-img"><img src="di/ic-calendar.png" height="80"></i>
							<p class="col-12 text-center text-medium text-gray mt-3">Please Select Reconcile Condition</p>
						</div>
					</div>-->
					
					<div id="ds-result" class="main-result">
						<!-- Reconcile balance -->
						<div class="bgi-hl">
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">E-Stamp</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
								</div>
								<div class="card bg-success">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">GL</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
							   </div>
							   <div class="card bg-success">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">&nbsp;</h3>
								</div>
							   <div class="card bg-success">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Diff</p>
												<p class="font-weight-normal text-white  text-large mb-4 value">&nbsp;</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-white mb-1 value">0.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">0</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
						</div>
						<!-- Reconcile balance -->
						
						<!-- Reconcile Unbalance -->
						<!--<div class="bgi-hl">
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">E-Stamp</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
								</div>
								<div class="card bg-danger">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">GL</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
							   </div>
							   <div class="card bg-danger">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">7,300</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">99</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">0.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">&nbsp;</h3>
								</div>
							   <div class="card bg-danger">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Diff</p>
												<p class="font-weight-normal text-white  text-large mb-4 value">&nbsp;</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">1</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
						</div>-->
						<!-- Reconcile Unbalance -->
						
						
						
						<div class="card mt-4">
						<div class="d-flex table-compare text-small">
							<div class="col-sm-7 p-0">
								<div class="card-header pt-2 pb-2">
									<h3 class="text-small mb-0 text-primary">E-Stamp Data</h3>
								</div>
								<div class="card-body p-0">
									<div class="mb-0">


									</div>
									<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer data-tables-hide-filter">

									   <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
									   <table class="data-table data-table-standard responsive nowrap skin-tmb top-rounded-0">

												<thead>
													<tr>
														<th class="text-white">Contract No</th>
														<th class="text-white">Duty Amount</th>
														<th class="text-white">Charge Amount</th>
														<th class="text-white">Total </th>
														<th class="text-white">Import Date</th>
														<th class="text-white">Contract Aging</th>

													</tr>
												</thead>
												<tbody>
													<?php for($i=1;$i<=20;$i++){ ?>
													<tr>
														<td>con2020032500<?php echo $i+4; ?></td>
														<td>500.00</td>
														<td>100.00</td>
														<td>600.00</td>
														<td>1-Jun-2020</td>
														<td>15</td>														
													</tr>
													<?php } ?>


												</tbody>
											</table>



									</div>

									<div class="mt-4">

									<div class="d-flex justify-content-end align-items-center">
											<div class="dropdown-as-select display-page" id="pageCount">
												<span class="text-light text-small">Rows per page </span>
												<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
													data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													10
												</button>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="#">5</a>
													<a class="dropdown-item active" href="#">10</a>
													<a class="dropdown-item" href="#">20</a>
												</div>
											</div>
											<div class="d-block d-md-inline-block ml-5">
												<nav class="ctrl-page d-flex flex-nowrap align-items-center">
													<span> 1-10 of 40</span>
													<ul class="pagination justify-content-center mb-0">
													   <!-- <li class="page-item ">
															<a class="page-link first" href="#">
																<i class="simple-icon-control-start"></i>
															</a>
														</li>-->
														<li class="page-item ">
															<a class="page-link prev" href="#">
																<i class="simple-icon-arrow-left"></i>
															</a>
														</li>
														<!--<li class="page-item active">
															<a class="page-link" href="#">1</a>
														</li>
														<li class="page-item ">
															<a class="page-link" href="#">2</a>
														</li>
														<li class="page-item">
															<a class="page-link" href="#">3</a>
														</li>-->
														<li class="page-item ">
															<a class="page-link next" href="#" aria-label="Next">
																<i class="simple-icon-arrow-right"></i>
															</a>
														</li>
														<!--<li class="page-item ">
															<a class="page-link last" href="#">
																<i class="simple-icon-control-end"></i>
															</a>
														</li>-->
													</ul>
												</nav>
											</div>


										</div>

									 </div>




								</div>
							</div>
							<div class="col-sm-5 border-left p-0">
								<div class="card-header pt-2 pb-2">
									<h3 class="text-small mb-0 text-primary">GL Data</h3>
								</div>
								<div class="card-body p-0">
									<div class="mb-0">


									</div>
									<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer data-tables-hide-filter">

									   <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
									   <table class="data-table data-table-standard responsive nowrap skin-tmb top-rounded-0">

												<thead>
													<tr>
														<th class="text-white">GL Date</th>
														<th class="text-white">GL Account</th>
														<th class="text-white">Duty Amount</th>
														<th class="text-white">Charge Amount</th>

													</tr>
												</thead>
												<tbody>
													<?php for($i=1;$i<=20;$i++){ ?>
													<tr>
														<td>9-Jul-2020</td>
														<td>SB00<?php echo $i; ?></td>				
														<td>100.00</td>
														<td>600.00</td>	
													</tr>
													<?php } ?>


												</tbody>
											</table>



									</div>

									<div class="mt-4">

									<div class="d-flex justify-content-end align-items-center">
											<div class="dropdown-as-select display-page" id="pageCount">
												<span class="text-light text-small">Rows per page </span>
												<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
													data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													10
												</button>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="#">5</a>
													<a class="dropdown-item active" href="#">10</a>
													<a class="dropdown-item" href="#">20</a>
												</div>
											</div>
											<div class="d-block d-md-inline-block ml-5">
												<nav class="ctrl-page d-flex flex-nowrap align-items-center">
													<span> 1-10 of 40</span>
													<ul class="pagination justify-content-center mb-0">
														<li class="page-item ">
															<a class="page-link prev" href="#">
																<i class="simple-icon-arrow-left"></i>
															</a>
														</li>
														<li class="page-item ">
															<a class="page-link next" href="#" aria-label="Next">
																<i class="simple-icon-arrow-right"></i>
															</a>
														</li>
	
													</ul>
												</nav>
											</div>


										</div>

									 </div>




								</div>
							</div>
						</div>	
						</div>
						
						
					</div>
					
                </div>
            </div>
        </div>

    </main>

    <?php include("incs/popup.html") ?>

    <?php include("incs/js.html") ?>
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
	
	
	

} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
//$(".sub-menu .list-unstyled:nth-child(2)>li:nth-child(1)").addClass('active');
</script>
</body>

</html>