<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Stamp</h1>
						
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Process Monitor</li>
							</ol>
						</nav>

                    </div>
					
					<div class="srh-bar mb-4 d-flex justify-content-between flex-row flex-nowrap">
						<div class="col p-0 pl-1 pr-0">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions" role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="searchOptions">
								<div class="d-flex flex-wrap justify-content-between h-100 align-items-end">
									<div class="main-srh col-xs-12 col-sm p-0 mr-3 mb-1">

										<div class="input-group">
											
											<input type="text" class="form-control bg-transparent" placeholder="Search by Request ID , Apiref No">
											<div class="input-group-append position-absolute">
												<button type="button" class="btn bg-transparent border-0"><img src="di/ic-search.png" height="22"></button>
											</div>
										</div>
									</div>
									
									<div class="col-sm-6 mb-1">
											
											<div class="row mb-0 align-items-center">
												<div class="col pr-2 form-group mb-0">
													<label>Start Record Date</label>
													<div class="input-group date">
															<span class="input-group-text input-group-append input-group-addon">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control" name="start" placeholder="Start date" value="10 Jun 2020">
														</div>

												</div>
												<div class="font-weight-bold pt-4">To</div>
												<div class="col pl-2 form-group mb-0">
													<label>End Record Date</label>
													<div class="input-group date">
															<span class="input-group-text input-group-append input-group-addon">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control" name="end" placeholder="End date" value="18 Jun 2020">
														</div>
												</div>
											</div>
										</div>
									
									

																		
									<div class="float-md-left d-flex align-items-center mb-1 col-xs-12 col-sm-3 col-xxl-2">
										
										<button class="btn btn-warning btn-md btn-block top-right-button rounded-05 mr-1" type="button" id="btnSearch"> <i class="icon-img"><img src="di/ic-search-wh.png" height="20"></i> Search</button>
									</div>

								</div>
							</div>
						</div>

					</div>



					
					
						<ul class="nav nav-tabs separator-tabs ml-0 mb-5 border-bottom-0" role="tablist">
							<li class="nav-item">
								<a class="nav-link " id="first-tab" data-toggle="tab" href="#first" role="tab"
									aria-controls="first" aria-selected="false">Auto</a>
							</li>
							
							<li class="nav-item"><span class="nav-link ">|</span></li>

							<li class="nav-item">
								<a class="nav-link active" id="second-tab" data-toggle="tab" href="#second" role="tab"
									aria-controls="second" aria-selected="true">Manual</a>
							</li>


						</ul>
						<div class="tab-content">
							<div class="tab-pane fade" id="first" role="tabpanel" aria-labelledby="first-tab">
								<div class="card">
								<div class="card-body p-0">
									<div class="p-5">tab1</div>
								</div>
								</div>
						 	</div>
							
							<!-- tab2 -->
							<div class="tab-pane show active" id="second" role="tabpanel" aria-labelledby="second-tab">
							
								<div class="card">
								<div class="card-body p-0">
									<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

								   <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
								   <table  class="data-table data-table-scrollable responsive nowrap skin-tmb">

											<thead>
												<tr>
													<th class="text-white sort-none">Process Name</th>
													<th class="text-white sort-none">Create</th>										
													<th class="text-white sort-none">Last Process</th>
													<th class="text-white sort-none">Success</th>
													<th class="text-white sort-none">Error</th>
													<th class="text-white sort-none">Total</th>
													<th class="text-white sort-none">Status</th>
													<th class="text-white text-center sort-none">Action</th>

												</tr>
											</thead>
											<tbody>
												<?php for($i=1;$i<=10;$i++){ ?>
												<tr>
													<td>Transfer staging (4 rows) : test_upload2.xlsx to generate document (4 items)</td>
													<td>05/08/2020<br>17:44:03</td>
													<td>05/08/2020<br> 17:44:03</td>
													<td>4</td>
													<td>0</td>
													<td>4</td>

													<td><?php if($i<2){ ?><span class="text-success">Completed</span> 
														<?php } elseif($i==3) { ?><span class="text-warning">Partial completed</span> 
														<?php } elseif($i==4) { ?><span class="text-danger">Error</span> 
														<?php } elseif($i==5) { ?><span class="text-primary">Process</span> 
														<?php } elseif($i==6) { ?><span class="text-black-50">Pause</span> 
														<?php } else { ?><span class="text-success">Completed</span> 
														<?php } ?>
													</td>
													<td class=" text-center">
														<?php if($i==5){ ?><a href="javascript:;"  class="btn btn-xs p-1 active" title="Play">
															<i class="icon-img"><img src="di/ic-process-play.png" height="24"></i>
														</a>
														<?php } elseif($i==6) { ?>
														<a href="javascript:;"  class="btn btn-xs p-1 active" title="Pause">
															<i class="icon-img"><img src="di/ic-process-pause.png" height="24"></i>
														</a>
														<?php } ?>
													</td>

												</tr>
												<?php } ?>


											</tbody>
										</table>



								</div>

									<div class="mt-4">

								<div class="d-flex justify-content-end align-items-center">
										<div class="dropdown-as-select display-page" id="pageCount">
											<span class="text-light text-small">Rows per page </span>
											<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
												data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												10
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">5</a>
												<a class="dropdown-item active" href="#">10</a>
												<a class="dropdown-item" href="#">20</a>
											</div>
										</div>
										<div class="d-block d-md-inline-block ml-5">
											<nav class="ctrl-page d-flex flex-nowrap align-items-center">
												<span> 1-10 of 40</span>
												<ul class="pagination justify-content-center mb-0">
												   <!-- <li class="page-item ">
														<a class="page-link first" href="#">
															<i class="simple-icon-control-start"></i>
														</a>
													</li>-->
													<li class="page-item ">
														<a class="page-link prev" href="#">
															<i class="simple-icon-arrow-left"></i>
														</a>
													</li>
													<!--<li class="page-item active">
														<a class="page-link" href="#">1</a>
													</li>
													<li class="page-item ">
														<a class="page-link" href="#">2</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>-->
													<li class="page-item ">
														<a class="page-link next" href="#" aria-label="Next">
															<i class="simple-icon-arrow-right"></i>
														</a>
													</li>
													<!--<li class="page-item ">
														<a class="page-link last" href="#">
															<i class="simple-icon-control-end"></i>
														</a>
													</li>-->
												</ul>
											</nav>
										</div>


									</div>

								 </div>

								 </div>
								 </div>
							</div>
						</div>
						
						
		
                </div>
            </div>
        </div>

    </main>


<div class="modal fade" id="addRoleModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-primary">Add Role</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-0 text-left">
			   <div class="row mt-4">
				   <div class="col-8 form-group">
					 <label>Role Name<span class="text-danger">*</span></label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Select Role Name*">
					 <!--<div class="input-group date">
						<span class="input-group-text input-group-append input-group-addon">
							<i class="simple-icon-calendar"></i>
						</span>
						<input type="text" class="input-sm form-control" name="start" placeholder="Start date" value="08/06/2020" />
					</div>-->
				   </div>
				   <div class="col form-group">
					 <label>Status</label>
					 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Status">
						<option></option>
						<option selected>Active</option>
						<option>Deactive</option>
					</select>
				  </div>
			  </div>
			  
			  <div class="wrap-role-user">

					   <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
					   <table class="table table-responsive-xs">
                                <thead class="bg-primary">
									<tr>
										<th class="text-white">All</th>
										<th class="text-white text-center pl-0">Status</th>
										<th class="text-white text-center pl-0">Add</th>
										<th class="text-white text-center pl-0">Edit</th>
										<th class="text-white text-center pl-0">Delete</th>
										<th class="text-white text-center pl-0">View</th>

									</tr>
								</thead>
								<tbody>
									<tr class="bg-light p-0">
										<td class="pt-1 pb-1 pl-4" colspan="6">
											<strong class="text-black ml-3">Functional</strong>
										</td>
										
									</tr>
									<tr>
										<td class="pb-2 border-bottom-0">
											<div class="custom-control custom-checkbox mb-0">
												<input type="checkbox" class="custom-control-input" id="customCheckAllrow">
												<label class="custom-control-label" for="customCheckAllrow">Import E-Stamp - As = TTB</label>
											</div>
										</td>
										
										<td class="text-center pb-2 border-bottom-0">&nbsp;</td>	
										<td class="text-center pl-2 pb-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
									</tr>
									<tr>
										<td class="pb-2 border-bottom-0">
											<div class="custom-control custom-checkbox mb-0">
												<input type="checkbox" class="custom-control-input" id="customCheckAllrow">
												<label class="custom-control-label" for="customCheckAllrow">Import E-Stamp - As = TTB</label>
											</div>
										</td>
										
										<td class="text-center pb-2 border-bottom-0">&nbsp;</td>	
										<td class="text-center pl-2 pb-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
									</tr>

									
									<tr class="bg-light p-0 m1-3">
										<td class="pt-1 pb-1 pl-4" colspan="6">
											<strong class="text-black ml-3">Report</strong>
										</td>
										
									</tr>
								</tbody>
						</table>
			</div>
			 
			   
			</div>
			<div class="modal-footer pt-3 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-outline-dark rounded-05 col-3 mr-4" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3 ml-4" data-dismiss="modal" data-toggle="modal" data-target="#payModalsuccess">Submit</button>
			</div>
		</div>
	</div>
</div>
<!-- /Modal -->
    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
} );
	</script>
			<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(9)").addClass('active');
//$(".sub-menu .list-unstyled:nth-child(4)>li:nth-child(3)").addClass('active');
</script>
</body>

</html>