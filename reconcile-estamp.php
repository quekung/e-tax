<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Stamp</h1>
						<!--<div class="top-right-button-container">
                        	<a href="reconcile-firstpage.php" class="btn btn-warning top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-reconcile.png" height="16"></i> RECONCILE</a>
                    	</div>-->

						
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Reconcile E-Stamp</li>
							</ol>
						</nav>

                    </div>

                    <div class="srh-bar mb-4 d-flex justify-content-between flex-row flex-nowrap">
						<div class="col p-0 pl-1 pr-0">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions"
								role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="searchOptions">
								<div class="d-flex flex-wrap justify-content-between h-100">
									<div class="main-srh col-xs-12 col-sm p-0 mr-3 mb-1">

										<div class="input-group">
											
											<input type="text" class="form-control bg-transparent" placeholder="Search by Request ID , Apiref No">
											<div class="input-group-append position-absolute">
												<button type="button" class="btn bg-transparent border-0"><img src="di/ic-search.png" height="22"></button>
											</div>
										</div>
									</div>

																		
									<div class="float-md-left d-flex align-items-center">
										
										<button class="btn btn-outline-warning btn-md top-right-button rounded-05 mr-1" type="button" id="filterMenuButton" data-toggle="collapse" href="#filterMenuDropdown" role="button" aria-expanded="false" aria-controls="filterMenuDropdown"> <i class="icon-img"><img src="di/ic-filter.png" height="20"></i> FILTER LIST</button>
										<div class="position-absolute collapse multi-collapse" id="filterMenuDropdown">
											<div class="card mb-4">
												<div class="card-body p-3">
													<h5 class="mb-2 h6">Submitted Date</h5>

													<div class="row mb-3">
														<div class="col-6 pr-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="start" placeholder="Start date" />
																</div>

														</div>
														<div class="col-6 pl-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="end" placeholder="End date" />
																</div>
														</div>
													</div>
													
													<h5 class="mb-2 h6">Payment Date</h5>

													<div class="row mb-3">
														<div class="col-6 pr-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="start" placeholder="Start date" />
																</div>

														</div>
														<div class="col-6 pl-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="end" placeholder="End date" />
																</div>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-12 mb-3">
															<label class="hid">Instrument type</label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="Instrument type">
																<option></option>
																<option>option 1</option>
																<option>option 2</option>
															</select>
														</div>
														
														
														<div class="col-sm-12 mb-3">
															<label class="hid">Payment Status</label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="Payment Status">
																<option></option>
																<option>option 1</option>
																<option>option 2</option>
															</select>
														</div>
										
													</div>

													<div class="row">
														<div class="col-6 pr-2">
															<button type="button" class="btn btn-block btn-outline-primary rounded-05">ยกเลิก</button>

														</div>
														<div class="col-6 pl-2">
															<button type="button" class="btn btn-block btn-primary rounded-05">ยืนยัน</button>
														</div>
													</div>


												</div>
											</div>

										</div>
									</div>

								</div>
							</div>
						</div>

					</div>
					
					<div class="topbar-sendmail d-flex justify-content-start align-items-center mb-2">
						<div class="btn-group mr-2">
							<div class="btn btn-xs btn-transparent pr-0 check-button">
								<label class="custom-control custom-checkbox mb-0 d-inline-block">
									<input type="checkbox" class="custom-control-input" id="ckAllDataTables">
									<span class="custom-control-label">&nbsp;</span>
								</label>
							</div>
							<button type="button" class="btn btn-xs btn-transparent dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item" href="#">Check All</a>
								<a class="dropdown-item" href="#">None</a>
							</div>
						</div>
						<div class="label-select-display mr-4">
						10 of 10
						</div>
						
						<a href="javascript:;" data-toggle="modal" data-target="#sendModalconfirm" title="Send E-Mail" class="btn btn-sm btn-transparent font-bold"><i class="icon-img mr-1"><img src="di/ic-bath-pay.png" height="14"></i> Payment</a>
					</div>
					
					


					<div class="card">
					<div class="card-body p-0">
						<div class="mb-0">
		
							
						</div>
						<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer data-tables-hide-filter">

                           <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
						   <table id="datatableCheck" class="data-table data-table-scrollable responsive nowrap skin-tmb">
							
									<thead>
										<tr>
											<th class="text-white pl-3 pr-0 sort-none">&nbsp;</th>
											<th class="text-white">Request ID</th>
											<th class="text-white">User ID</th>
											<th class="text-white">Count</th>
											<th class="text-white">Total Amount</th>
											<th class="text-white">Submitted Date</th>
											<th class="text-white">Payment Date </th>
											<th class="text-white">Source</th>
											<th class="text-white">Instrument</th>
											<th class="text-white">Status</th>
											<th class="text-white sort-none text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=10;$i++){ ?>
										<tr>
											<td class="pl-3">												
												<label class="custom-control custom-checkbox mb-1 align-self-center data-table-rows-check">
													<input type="checkbox" class="custom-control-input">
													<span class="custom-control-label">&nbsp;</span>
												</label>
											</td>
											<td>rdtest000014<?php echo $i; ?></td>
											<td><?php if($i%3==0){ ?>Mana<?php } else { ?>Manee<?php } ?></td>
											<td>85</td>
											<td>
												<?php if($i%3==0){ ?>8,000 <?php } elseif ($i%5==0) { ?>6,000 <?php } else { ?>5,000<?php } ?>
											</td>
											<td>1-Jun-2020</td>
											<td>1-Jun-2020</td>
											<td><?php if($i%3==0){ ?>ALS <?php } elseif ($i%5==0) { ?>TAP <?php } else { ?>EXIM <?php } ?></td>
											<td>04</td>
											<td>
												<?php if($i<=1){ ?><span class="text-success">Success</span>
												<?php } elseif ($i<=3) { ?><span class="text-primary">Wait for RD<br> Response</span>
												<?php } elseif ($i<=6) { ?><span class="text-danger">Payment Fail<br>(Retry pay)</span> 
												<?php } else { ?><span class="text-warning">Wait for pay</span> <?php } ?>
											</td>
											
											<td class=" text-center">
												<?php if($i<=1){ ?><a href="list-reconcile.php" class="btn  btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-view.png" height="16"></i></a> 
												<?php } elseif ($i<=3) { ?><a href="list-reconcile-fail.php" class="btn  btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-view.png" height="16"></i></a> 
												<?php } elseif ($i<=6) { ?><a href="list-reconcile-fail.php" class="btn  btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-view.png" height="16"></i></a> 
												<?php } else { ?><a href="list-reconcile-fail.php" class="btn  btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-view.png" height="16"></i></a> <?php } ?>
												
												<a href="javascript:;" data-toggle="modal" data-target="#qrSlipModal" class="btn  btn-xs p-1 <?php if($i<=5){ ?>invisibled<?php } else { ?>active<?php } ?>" title="QR"><i class="icon-img"><img src="di/ic-qr.png" height="20"></i></a> 
												<a href="javascript:;" data-toggle="modal" <?php if($i>=5 && $i<=6){ ?>data-target="#RetryPayModalconfirm"<?php } else { ?>data-target="#PromptpayModalconfirm"<?php } ?> class="btn  btn-xs p-1 <?php if($i>=2 && $i<=4){ ?>invisibled<?php } else { ?>active<?php } ?>" title="retry"><i class="icon-img"><img src="di/ic-retry.png" height="16"></i></a> 
											</td>
										</tr>
										<?php } ?>
							

									</tbody>
								</table>
								
		

						</div>
						
						<div class="mt-4">
                 
						<div class="d-flex justify-content-end align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-light text-small">Rows per page </span>
									<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
										data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										10
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">5</a>
										<a class="dropdown-item active" href="#">10</a>
										<a class="dropdown-item" href="#">20</a>
									</div>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<span> 1-10 of 40</span>
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<!--<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
									</nav>
								</div>
								
								
							</div>
						
                   		 </div>
						
						
						
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

    <?php include("incs/popup.html") ?>

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
	//table check list
	
	$('#datatableCheck tbody').on('click', 'tr', function () {

        $(this).toggleClass('selected');

        var $checkBox = $(this).find(".custom-checkbox input");

        $checkBox.prop("checked", !$checkBox.prop("checked")).trigger("change");

        controlCheckAll();

      });



      function controlCheckAll() {

        var anyChecked = false;

        var allChecked = true;

        $('#datatableCheck tbody tr .custom-checkbox input').each(function () {

          if ($(this).prop("checked")) {

            anyChecked = true;

          } else {

            allChecked = false;

          }

        });

        if (anyChecked) {

          $("#ckAllDataTables").prop("indeterminate", anyChecked);

        } else {

          $("#ckAllDataTables").prop("indeterminate", anyChecked);

          $("#ckAllDataTables").prop("checked", anyChecked);

        }

        if (allChecked) {

          $("#ckAllDataTables").prop("indeterminate", false);

          $("#ckAllDataTables").prop("checked", allChecked);

        }

      }

	/**/
	function unCkAllRows() {

        $('#datatableCheck tbody tr').removeClass('selected');

        $('#datatableCheck tbody tr .custom-checkbox input').prop("checked", false).trigger("change");

      }



      function ckAllRows() {

        $('#datatableCheck tbody tr').addClass('selected');

        $('#datatableCheck tbody tr .custom-checkbox input').prop("checked", true).trigger("change");

      }



      $("#ckAllDataTables").on("click", function (event) {

        var isCheckedAll = $("#ckAllDataTables").prop("checked");

        if (isCheckedAll) {

          ckAllRows();

        } else {

          unCkAllRows();

        }

      });

} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
$(".sub-menu .list-unstyled:nth-child(2)>li:nth-child(1)").addClass('active');
</script>
</body>

</html>