<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Report</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<!--<ol class="breadcrumb pt-0">
								<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Infomation estamp</li>
							</ol>-->
						</nav>
                        <!--<div class="top-right-button-container">

                            <div class="btn-group">
                                <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                    <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                        <input type="checkbox" class="custom-control-input" id="checkAll">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </div>
                                <button type="button"
                                    class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </div>-->
                    </div>

                    <div class="srh-bar mb-4 d-flex justify-content-between flex-row flex-nowrap">
						<div class="card col p-0 pl-3 pr-0 mr-3">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
								role="button" aria-expanded="true" aria-controls="displayOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block h-100" id="displayOptions">
								<div class="d-flex justify-content-between h-100">
									<div class="col p-0 mr-3 mb-1">
										<label class="d-block mb-0">&nbsp;</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="simple-icon-magnifier"></i></div>
											</div>
											<input type="text" class="form-control form-control-sm" placeholder="Search">
										</div>
									</div>
									
									<div class="float-md-left mr-3 mb-1 dropdown-as-select">
										<label class="d-block mb-0">Start date :</label>
										<div class="input-group date">
                                            <input type="text" class="form-control form-control-sm" placeholder="5/31/2020">
                                            <span class="input-group-text input-group-append input-group-addon">
                                                <i class="simple-icon-calendar"></i>
                                            </span>
                                        </div>
									</div>
									<div class="float-md-left mr-3 mb-1 dropdown-as-select">
										<label class="d-block mb-0">End date :</label>
										<div class="input-group date">
                                            <input type="text" class="form-control form-control-sm" placeholder="5/31/2020">
                                            <span class="input-group-text input-group-append input-group-addon">
                                                <i class="simple-icon-calendar"></i>
                                            </span>
                                        </div>
									</div>

																		
									<div class="float-md-left d-flex align-items-center h-100">
										
										<button type="button" class="btn btn-gray rounded-1 btn-lg text-white h-100" data-toggle="modal" data-backdrop="static" data-target="#exampleModal"><i class="simple-icon-magnifier"></i> Search</button>
									</div>

								</div>
							</div>
						</div>
						<div class="col-r top-right-button-container d-flex align-items-center">
                            <!--<button type="button" class="btn btn-primary btn-md top-right-button  mr-1"
                                data-toggle="modal" data-backdrop="static" data-target="#exampleModal">+ Add</button>-->
								<a class="btn btn-green btn-lg top-right-button rounded-1 mr-1" href="add-dataconsent.php"> <i class="glyph-icon iconsminds-add"></i> Add</a>
						</div>
					</div>


					<div class="card">
					<div class="card-body">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
							<!--<table class="data-table data-tables-pagination responsive nowrap" data-order="[[ 1, &quot;desc&quot; ]]">-->
                           <table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >
							
									<thead>
										<tr>
											<th class="text-orange text-center">Issue Date Time</th>
											<th class="text-orange">Seller Name</th>
											<th class="text-orange text-center">Branch Code</th>
											<th class="text-orange">Document Type</th>
											<th class="text-orange text-center">Total Bill</th>
											<th class="text-orange text-center">PDF</th>
											<th class="text-orange text-center">XML</th>
											<th class="text-orange text-center">Amount</th>
											<th class="text-orange text-center">Text Amount</th>
											<th class="text-orange sort-none text-right">Grand Total Amount</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=10;$i++){ ?>
										<tr>
											<td class="text-center">2020-06</td>
											<td>iRich Co.ltd., สำนักงานใหญ่</td>
											<td class="text-center">
												0000<?php echo $i ?>
											</td>
											<td>ใบแจ้งหนี้/ใบกำกับภาษี</td>
											<td class="text-center">1</td>
											<td class="text-center">1</td>
											<td class="text-center">1</td>
											<td class="text-center">18,000.00</td>
											<td class="text-center">1,260.00</td>
											<td class="text-right">
												19,260.00 
											</td>
										</tr>
										<?php } ?>
							

									</tbody>
								</table>
								
		

						</div>
						<nav class="mt-4 mb-3">
                        <ul class="pagination justify-content-center mb-0">
                            <li class="page-item ">
                                <a class="page-link first" href="#">
                                    <i class="simple-icon-control-start"></i>
                                </a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link prev" href="#">
                                    <i class="simple-icon-arrow-left"></i>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">3</a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link next" href="#" aria-label="Next">
                                    <i class="simple-icon-arrow-right"></i>
                                </a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link last" href="#">
                                    <i class="simple-icon-control-end"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
						
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
} );
	</script>
</body>

</html>