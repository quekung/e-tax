<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Stamp</h1>
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Reconcile E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Reconcile</li>
							</ol>
						</nav>

                    </div>

                    <div class="srh-bar mb-4 d-flex justify-content-between flex-row flex-nowrap">
						<div class="col p-0 pl-1 pr-0">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions"
								role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="searchOptions">
								<div class="card d-flex flex-wrap justify-content-between h-100">
									<div class="card-body reconcile-srh col-xs-12 col-sm mb-4">
										
										<div class="custom-control custom-checkbox mb-3">
											<input type="checkbox" class="custom-control-input" id="customCheckThis">
											<label class="custom-control-label" for="customCheckThis">Use the same search date</label>
										</div>
										
										<div class="row">
										<div class="col-sm-6 form-group mb-3">
											<label>Select Import Date</label>
											<div class="row mb-3 align-items-center">
												<div class="col pr-2">
													<div class="input-group date">
															<span class="input-group-text input-group-append input-group-addon">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control" name="start" placeholder="Start date" />
														</div>

												</div>
												<div class="font-weight-bold">To</div>
												<div class="col pl-2">
													<div class="input-group date">
															<span class="input-group-text input-group-append input-group-addon">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control" name="end" placeholder="End date" />
														</div>
												</div>
											</div>
										</div>
										
										<div class="col-sm-6 form-group mb-3">
											<label>Select GL Date</label>
											<div class="row mb-3 align-items-center">
												<div class="col pr-2">
													<div class="input-group date">
															<span class="input-group-text input-group-append input-group-addon">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control" name="start" placeholder="Start date" />
														</div>

												</div>
												<div class="font-weight-bold">To</div>
												<div class="col pl-2">
													<div class="input-group date">
															<span class="input-group-text input-group-append input-group-addon">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control" name="end" placeholder="End date" />
														</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row flex-wrap align-items-end">
										<div class="form-group mb-0 col-12 col-sm mb-3">
											<label>Tags</label>
											<input data-role="tagsinput" type="text">
										</div>

										<div class="top-right-button-container text-nowrap col-xs-12 col-sm-auto mb-3">
											<button type="button" class="btn btn-warning top-right-button rounded-05" onClick="$('#ds-default').fadeOut().addClass('hid'); $('#ds-result').fadeIn();"><i class="icon-img"><img src="di/ic-reconcile.png" height="16"></i> RECONCILE</button>
										</div>
									</div>


										
									</div>
								</div>
							</div>
						</div>

					</div>
					
	

					
					<div id="ds-default">
						<div class="display-default d-flex flex-wrap justify-content-center align-items-center p-5">
							<i class="icon-img"><img src="di/ic-calendar.png" height="80"></i>
							<p class="col-12 text-center text-medium text-gray mt-3">Please Select Reconcile Condition</p>
						</div>
					</div>
					
					<div id="ds-result" class="main-result hid">
						<!-- Reconcile balance -->
						<div class="bgi-hl">
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">E-Stamp</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
								</div>
								<div class="card bg-success">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">GL</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
							   </div>
							   <div class="card bg-success">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">&nbsp;</h3>
								</div>
							   <div class="card bg-success">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Diff</p>
												<p class="font-weight-normal text-white  text-large mb-4 value">&nbsp;</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-white mb-1 value">0.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">0</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
						</div>
						<!-- Reconcile balance -->
						
						<!-- Reconcile Unbalance -->
						<div class="bgi-hl">
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">E-Stamp</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
								</div>
								<div class="card bg-danger">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">GL</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
							   </div>
							   <div class="card bg-danger">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">7,300</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">99</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">0.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">&nbsp;</h3>
								</div>
							   <div class="card bg-danger">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Diff</p>
												<p class="font-weight-normal text-white  text-large mb-4 value">&nbsp;</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">1</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
						</div>
						<!-- Reconcile Unbalance -->
						
						<!-- group -->
						<div class="recon-panel mb-5">
							<div class="title-bar d-flex justify-content-between align-items-end mb-3">
								<h3 class="h6 mb-0">EXIM</h3>
								<a href="reconcile-list.php" class="btn btn-primary top-right-button rounded-05 text-small"><i class="icon-img"><img src="di/ic-view-wh.png" height="16"></i> VIEW SOURCE</a>
							</div>
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Grand Total</p>
												<p class="font-weight-bold text-success mb-2 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-success mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-success mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-success mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Grand Total</p>
												<p class="font-weight-bold text-success mb-2 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-success mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-success mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-success mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Diff</p>
												<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-success mb-1 value">0.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-success mb-1 value">0</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
                        </div>  
						<!-- group -->
						
						<!-- group Reconcile unbalance -->
						<div class="recon-panel mb-5">
							<div class="title-bar d-flex justify-content-between align-items-end mb-3">
								<h3 class="h6 mb-0">ALS</h3>
								<a href="reconcile-list.php" class="btn btn-primary top-right-button rounded-05 text-small"><i class="icon-img"><img src="di/ic-view-wh.png" height="16"></i> VIEW SOURCE</a>
							</div>
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Grand Total</p>
												<p class="font-weight-bold text-danger mb-2 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-danger mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-danger mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-danger mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Grand Total</p>
												<p class="font-weight-bold text-danger mb-2 value">7,300</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-danger mb-1 value">99</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-danger mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-danger mb-1 value">0.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Diff</p>
												<p class="font-weight-bold text-danger mb-2 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-danger mb-1 value">700.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-danger mb-1 value">1</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
                        </div>  
						<!-- group Reconcile unbalance -->
						
						<!-- group -->
						<div class="recon-panel mb-5">
							<div class="title-bar d-flex justify-content-between align-items-end mb-3">
								<h3 class="h6 mb-0">Card Link</h3>
								<a href="reconcile-list.php" class="btn btn-primary top-right-button text-small rounded-05 text-small"><i class="icon-img"><img src="di/ic-view-wh.png" height="16"></i> VIEW SOURCE</a>
							</div>
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Grand Total</p>
												<p class="font-weight-bold text-success mb-2 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-success mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-success mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-success mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Grand Total</p>
												<p class="font-weight-bold text-success mb-2 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-success mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-success mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-success mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Diff</p>
												<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-success mb-1 value">0.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-success mb-1 value">0</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
                        </div>  
						<!-- group -->
						
						<!-- group -->
						<div class="recon-panel mb-5">
							<div class="title-bar d-flex justify-content-between align-items-end mb-3">
								<h3 class="h6 mb-0">TAP</h3>
								<a href="reconcile-list.php" class="btn btn-primary top-right-button rounded-05 text-small"><i class="icon-img"><img src="di/ic-view-wh.png" height="16"></i> VIEW SOURCE</a>
							</div>
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Grand Total</p>
												<p class="font-weight-bold text-success mb-2 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-success mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-success mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-success mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Grand Total</p>
												<p class="font-weight-bold text-success mb-2 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-success mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-success mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-success mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="card">
									<div class="card-body p-3">

												<p class="mb-0 label text-medium">Diff</p>
												<p class="font-weight-bold text-success mb-2 value">&nbsp;</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-success mb-1 value">0.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-success mb-1 value">0</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
                        </div>  
						<!-- group -->
					</div>
					
                </div>
            </div>
        </div>

    </main>

    <?php include("incs/popup.html") ?>

    <?php include("incs/js.html") ?>
	<link rel="stylesheet" href="css/vendor/bootstrap-tagsinput.css" />
	 <script src="js/vendor/datatables.min.js"></script>
    <script src="js/vendor/bootstrap-tagsinput.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
	

} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
//$(".sub-menu .list-unstyled:nth-child(2)>li:nth-child(1)").addClass('active');
</script>
</body>

</html>