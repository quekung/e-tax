<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Stamp</h1>
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Reconcile E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Reconcile List</li>
							</ol>
						</nav>

                    </div>

                    <div class="card mb-4">
					<div class="card-body p-3 d-flex justify-content-between align-items-center">

						<h2 class="h5 mb-0 font-weight-bold">EXIM  Source system</h2>
						<a href="javascript:;" data-toggle="modal" data-target="#prepareModal" id="btn-pre-send2rd" class="btn btn-primary top-right-button rounded-05 btn-pre-send2rd disabled"><i class="icon-img"><img src="di/ic-send-to-rd.png" height="20"></i> SEND TO RD</a>

					</div>
				</div>
					
	

					
					<!--<div id="ds-default">
						<div class="display-default d-flex flex-wrap justify-content-center align-items-center p-5">
							<i class="icon-img"><img src="di/ic-calendar.png" height="80"></i>
							<p class="col-12 text-center text-medium text-gray mt-3">Please Select Reconcile Condition</p>
						</div>
					</div>-->
					
					<div id="ds-result" class="main-result">
						<!-- Reconcile balance -->
						<div class="bgi-hl">
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">E-Stamp</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
								</div>
								<div class="card bg-success">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">GL</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
							   </div>
							   <div class="card bg-success">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">&nbsp;</h3>
								</div>
							   <div class="card bg-success">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Diff</p>
												<p class="font-weight-normal text-white  text-large mb-4 value">&nbsp;</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-white mb-1 value">0.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">0</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
						</div>
						<!-- Reconcile balance -->
						
						<!-- Reconcile Unbalance -->
						<!--<div class="bgi-hl">
							<ul class="row chd-group list-inline">
							<li class="col-sm">
								<div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">E-Stamp</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
								</div>
								<div class="card bg-danger">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">8,000</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">100</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">GL</h3>
									<span class="text-black-50">10 Jun 2020 - 18 Jun 2020</span>
							   </div>
							   <div class="card bg-danger">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Grand Total</p>
												<p class="font-weight-normal text-white text-large mb-4 value">7,300</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col-4 g1">
														<p class="label text-small">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">99</p>
													</div>
													<div class="col-4 g2">
														<p class="label text-small">Total Duty</p>
														<p class="font-weight-bold text-white mb-1 value">7,300</p>
													</div>
													<div class="col-4 g3">
														<p class="label text-small">Total Charge</p>
														<p class="font-weight-bold text-white mb-1 value">0.00</p>
													</div>
												</div>
								   </div>
							   </div>
							   </li>
							   <li class="col-sm-auto">
							   <div class="title-bar d-flex justify-content-strat align-items-end mb-3">
									<h3 class="h5 mb-0 mr-3">&nbsp;</h3>
								</div>
							   <div class="card bg-danger">
									<div class="card-body text-white p-3">

												<p class="mb-2 label text-medium">Diff</p>
												<p class="font-weight-normal text-white  text-large mb-4 value">&nbsp;</p>
												<div class="separator mb-3"></div>
												<div class="row">
													<div class="col g1">
														<p class="label text-small text-nowrap">Grand Total</p>
														<p class="font-weight-bold text-white mb-1 value">700.00</p>
													</div>
													<div class="col g2">
														<p class="label text-small text-nowrap">Total Record</p>
														<p class="font-weight-bold text-white mb-1 value">1</p>
													</div>


												</div>
								   </div>
							   </div>
							 </li>
						   </ul>
						</div>-->
						<!-- Reconcile Unbalance -->
						
						
						<div class="topbar-sendmail d-flex justify-content-start align-items-center mb-2">
						<div class="btn-group mr-2">
							<div class="btn btn-xs btn-transparent pr-0 check-button">
								<label class="custom-control custom-checkbox mb-0 d-inline-block">
									<input type="checkbox" class="custom-control-input" id="ckAllDataTables">
									<span class="custom-control-label">&nbsp;</span>
								</label>
							</div>
							<button type="button" class="btn btn-xs btn-transparent dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item" href="#">Check All</a>
								<a class="dropdown-item" href="#">None</a>
							</div>
						</div>
						<div class="label-select-display mr-4">
						10 of 10
						</div>
						
						<!--<a href="javascript:;" data-toggle="modal" data-target="#qrSlipModal" title="Send E-Mail" class="btn btn-sm btn-gray2 rounded-05 font-bold"><i class="icon-img mr-1"><img src="di/ic-send-to-rd.png" height="14"></i> Send to RD</a>-->
					</div>
						
						<div class="card">
							<div class="card-header pt-2 pb-2">
								<h3 class="h6 mb-0 text-primary">E-Stamp Data</h3>
							</div>
							<div class="card-body p-0">
								<div class="mb-0">


								</div>
								<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer data-tables-hide-filter">

								   <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
								   <table id="datatableCheck" class="data-table data-table-standard responsive nowrap skin-tmb top-rounded-0">

											<thead>
												<tr>
													<th class="text-white pl-3 pr-0 sort-none">&nbsp;</th>
													<th class="text-white">Contract Date</th>
													<th class="text-white">Import Date</th>
													<th class="text-white">Contract No.</th>
													<th class="text-white">Customer </th>
													<th class="text-white">Charge</th>
													<th class="text-white">Summary</th>
													<th class="text-white">As</th>
													<th class="text-white">Remark</th>
					
												</tr>
											</thead>
											<tbody>
												<?php for($i=1;$i<=20;$i++){ ?>
												<tr>
													<td class="pl-3">												
														<label class="custom-control custom-checkbox mb-1 align-self-center data-table-rows-check">
															<input type="checkbox" class="custom-control-input">
															<span class="custom-control-label">&nbsp;</span>
														</label>
													</td>
													<td>9-Jul-2020</td>
													<td>10-Jul-2020</td>
													<td>con2020032500<?php echo $i+4; ?></td>
													<td>
														Sompong.A
													</td>
													<td>1,000.00</td>
													<td>1,000.00</td>
													<td>TMB</td>
													<td>-</td>
													
												</tr>
												<?php } ?>


											</tbody>
										</table>



								</div>

								<div class="mt-4">

								<div class="d-flex justify-content-end align-items-center">
										<div class="dropdown-as-select display-page" id="pageCount">
											<span class="text-light text-small">Rows per page </span>
											<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
												data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												10
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">5</a>
												<a class="dropdown-item active" href="#">10</a>
												<a class="dropdown-item" href="#">20</a>
											</div>
										</div>
										<div class="d-block d-md-inline-block ml-5">
											<nav class="ctrl-page d-flex flex-nowrap align-items-center">
												<span> 1-10 of 40</span>
												<ul class="pagination justify-content-center mb-0">
												   <!-- <li class="page-item ">
														<a class="page-link first" href="#">
															<i class="simple-icon-control-start"></i>
														</a>
													</li>-->
													<li class="page-item ">
														<a class="page-link prev" href="#">
															<i class="simple-icon-arrow-left"></i>
														</a>
													</li>
													<!--<li class="page-item active">
														<a class="page-link" href="#">1</a>
													</li>
													<li class="page-item ">
														<a class="page-link" href="#">2</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>-->
													<li class="page-item ">
														<a class="page-link next" href="#" aria-label="Next">
															<i class="simple-icon-arrow-right"></i>
														</a>
													</li>
													<!--<li class="page-item ">
														<a class="page-link last" href="#">
															<i class="simple-icon-control-end"></i>
														</a>
													</li>-->
												</ul>
											</nav>
										</div>


									</div>

								 </div>
							



							</div>
							</div>
						
					</div>
					
                </div>
            </div>
        </div>

    </main>

    <?php include("incs/popup.html") ?>

    <?php include("incs/js.html") ?>
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
	//table check list
	
	$('#datatableCheck tbody').on('click', 'tr', function () {

        $(this).toggleClass('selected');

        var $checkBox = $(this).find(".custom-checkbox input");

        $checkBox.prop("checked", !$checkBox.prop("checked")).trigger("change");

        controlCheckAll();

      });



      function controlCheckAll() {

        var anyChecked = false;

        var allChecked = true;

        $('#datatableCheck tbody tr .custom-checkbox input').each(function () {

          if ($(this).prop("checked")) {

            anyChecked = true;

          } else {

            allChecked = false;

          }

        });

        if (anyChecked) {

          $("#ckAllDataTables").prop("indeterminate", anyChecked);

        } else {

          $("#ckAllDataTables").prop("indeterminate", anyChecked);

          $("#ckAllDataTables").prop("checked", anyChecked);


        }

        if (allChecked) {

          $("#ckAllDataTables").prop("indeterminate", false);

          $("#ckAllDataTables").prop("checked", allChecked);

        }

      }

	/**/
	function unCkAllRows() {

        $('#datatableCheck tbody tr').removeClass('selected');

        $('#datatableCheck tbody tr .custom-checkbox input').prop("checked", false).trigger("change");
		
		$('.btn-pre-send2rd').addClass('disabled');

      }



      function ckAllRows() {

        $('#datatableCheck tbody tr').addClass('selected');

        $('#datatableCheck tbody tr .custom-checkbox input').prop("checked", true).trigger("change");
		
		$('.btn-pre-send2rd').removeClass('disabled');

      }



      $("#ckAllDataTables").on("click", function (event) {

        var isCheckedAll = $("#ckAllDataTables").prop("checked");

        if (isCheckedAll) {

          ckAllRows();

        } else {

          unCkAllRows();

        }

      });
	  
	  /*$('.custom-control-input').on("click", function (event) {
	  	$('.btn-pre-send2rd').removeClass('disabled');
	  });*/

} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
//$(".sub-menu .list-unstyled:nth-child(2)>li:nth-child(1)").addClass('active');
</script>
</body>

</html>