<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Stamp</h1>
						
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Report</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Reconcile DWH & GL Report</li>
							</ol>
						</nav>

                    </div>
					
					<div class="srh-bar mb-4 d-flex justify-content-between flex-row flex-nowrap">
						<div class="col p-0 pl-1 pr-0">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions" role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="card collapse d-md-block" id="searchOptions">
								<div class="card-body reconcile-srh p-3 d-flex flex-wrap justify-content-between h-100 align-items-end">
									<div class="d-flex w-100 mb-3">
									
										<div class="col-12 col-sm form-group mb-0">
											<label>Source</label>
											<input data-role="tagsinput" class="rounded-05" type="text" value="EXIM, ALS, Card Link, TAP,">
										</div>
										
										<div class="col-12 col-sm form-group mb-0">
											<label>Import Date</label>
											<div class="row mb-0 align-items-center">
												<div class="col pr-2">
													<div class="input-group date">
															<span class="input-group-text input-group-append input-group-addon">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control" name="start" placeholder="Start date">
														</div>

												</div>
												<div class="font-weight-bold">To</div>
												<div class="col pl-2">
													<div class="input-group date">
															<span class="input-group-text input-group-append input-group-addon">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control" name="end" placeholder="End date">
														</div>
												</div>
											</div>
										</div>
										
										
									</div>

									<div class="d-flex flex-wrap w-100 align-items-end">
										<div class="col-12 col-sm-auto form-group mb-2">
											<label>Import Status</label>
											<select class="form-control select2-normal" data-width="100%" data-placeholder="Select Import Status">
												<option></option>
												<option>option 1</option>
												<option>option 2</option>
											</select>

										</div>
										
										<!--<div class="col-12 col-sm form-group mb-2">
											<label>File name</label>
											<input type="text" class="input-sm form-control rounded-05" name="tax_id" placeholder="Specify File name">

										</div>-->


										<div class="top-right-button-container text-nowrap col-12 col-sm-auto mb-2">

											<button class="btn btn-primary btn-md top-right-button rounded-05" type="button" id="btnSearch" style="min-width: 120px"> <!--<i class="icon-img"><img src="di/ic-search-wh.png" height="20"></i>--> Search</button>
										</div>
										
									</div>

								</div>
							</div>
						</div>

					</div>



					
					<div class="headbar-tb mb-4 d-flex justify-content-end align-items-center">
						<div class="top-right-button-container">
							<a href="#" class="btn btn-transparent top-right-button rounded-05 mr-1 text-gray"> Download</a>
							<a href="#" class="btn btn-success top-right-button rounded-05 mr-1"><i class="icon-img f-white mt-1n"><img src="di/ic-download-fromupload.png" height="16"></i> CSV</a>
							<a href="#" class="btn btn-danger top-right-button rounded-05 mr-1"><i class="icon-img f-white mt-1n"><img src="di/ic-download-fromupload.png" height="16"></i> PDF</a>

						</div>
						
					</div>
					
					<div class="card">
					<div class="card-body p-0">

					<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

				   <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
				   <table  class="data-table data-table-scrollable responsive nowrap skin-tmb">

							<thead>
								
								<tr>
<th rowspan="2" class="text-white sort-none">Source</th>
<th rowspan="2" class="text-white sort-none">Import Date</th>
<th rowspan="2" class="text-white sort-none">File name</th>
<th rowspan="2" class="text-white sort-none">Import Status</th>
<th colspan="2" class="text-white sort-none pt-0 pb-0 text-center">Total Transaction <br>(จำนวนรายการ)</th>
<th colspan="2" class="text-white sort-none pt-0 pb-0 text-center">Total Amount <br>(จำนวนเงินรวม)</th>
<th colspan="2" class="text-white sort-none pt-0 pb-0 text-center">Error</th>
</tr>
<tr>
	<th class="text-white sort-none bg-gray2 text-center">DWH</th>	
	<th class="text-white sort-none bg-gray2 text-center">E-Stamp</th>
	<th class="text-white sort-none bg-gray2 text-center">DWH</th>	
	<th class="text-white sort-none bg-gray2 text-center">E-Stamp</th>
	<th class="text-white sort-none bg-gray2 text-center">Transaction</th>	
	<th class="text-white sort-none bg-gray2 text-center">Amount</th>

</tr>
							</thead>
							<tbody>
								<?php for($i=1;$i<=10;$i++){ ?>
								<tr>
									
									<td>
										<?php if($i%2==0){ ?>ALS
										<?php } else { ?>Exim<?php } ?>
										
									</td>
									<td>7/10/20</td>
									<td>ABCDEF</td>
									<td><?php if($i%3==0){ ?><span class="text-danger">ไม่สำเร็จ</span> 
										<?php } else { ?><span class="text-success">สำเร็จ</span><?php } ?></td>
									
									<td>300</td>
									<td>200</td>
									<td>1,000,000.00</td>
									<td>500,000.00</td>
									<td><?php if($i%5==0){ ?>443<?php } else { ?>-<?php } ?></td>
									<td><?php if($i%5==0){ ?>500,000.00<?php } else { ?>-<?php } ?></td>
									
									


									
									


								</tr>
								<?php } ?>


							</tbody>
						</table>



				</div>

					<div class="mt-4">

				<div class="d-flex justify-content-end align-items-center">
						<div class="dropdown-as-select display-page" id="pageCount">
							<span class="text-light text-small">Rows per page </span>
							<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
								data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								10
							</button>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item" href="#">5</a>
								<a class="dropdown-item active" href="#">10</a>
								<a class="dropdown-item" href="#">20</a>
							</div>
						</div>
						<div class="d-block d-md-inline-block ml-5">
							<nav class="ctrl-page d-flex flex-nowrap align-items-center">
								<span> 1-10 of 40</span>
								<ul class="pagination justify-content-center mb-0">
								   <!-- <li class="page-item ">
										<a class="page-link first" href="#">
											<i class="simple-icon-control-start"></i>
										</a>
									</li>-->
									<li class="page-item ">
										<a class="page-link prev" href="#">
											<i class="simple-icon-arrow-left"></i>
										</a>
									</li>
									<!--<li class="page-item active">
										<a class="page-link" href="#">1</a>
									</li>
									<li class="page-item ">
										<a class="page-link" href="#">2</a>
									</li>
									<li class="page-item">
										<a class="page-link" href="#">3</a>
									</li>-->
									<li class="page-item ">
										<a class="page-link next" href="#" aria-label="Next">
											<i class="simple-icon-arrow-right"></i>
										</a>
									</li>
									<!--<li class="page-item ">
										<a class="page-link last" href="#">
											<i class="simple-icon-control-end"></i>
										</a>
									</li>-->
								</ul>
							</nav>
						</div>


					</div>

				 </div>
				 	
					</div>
					</div>
				
						
						
		
                </div>
            </div>
        </div>

    </main>


<div class="modal fade" id="addRoleModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-primary">Add Role</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-0 text-left">
			   <div class="row mt-4">
				   <div class="col-8 form-group">
					 <label>Role Name<span class="text-danger">*</span></label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Select Role Name*">
					 <!--<div class="input-group date">
						<span class="input-group-text input-group-append input-group-addon">
							<i class="simple-icon-calendar"></i>
						</span>
						<input type="text" class="input-sm form-control" name="start" placeholder="Start date" value="08/06/2020" />
					</div>-->
				   </div>
				   <div class="col form-group">
					 <label>Status</label>
					 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Status">
						<option></option>
						<option selected>Active</option>
						<option>Deactive</option>
					</select>
				  </div>
			  </div>
			  
			  <div class="wrap-role-user">

					   <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
					   <table class="table table-responsive-xs">
                                <thead class="bg-primary">
									<tr>
										<th class="text-white">All</th>
										<th class="text-white text-center pl-0">Status</th>
										<th class="text-white text-center pl-0">Add</th>
										<th class="text-white text-center pl-0">Edit</th>
										<th class="text-white text-center pl-0">Delete</th>
										<th class="text-white text-center pl-0">View</th>

									</tr>
								</thead>
								<tbody>
									<tr class="bg-light p-0">
										<td class="pt-1 pb-1 pl-4" colspan="6">
											<strong class="text-black ml-3">Functional</strong>
										</td>
										
									</tr>
									<tr>
										<td class="pb-2 border-bottom-0">
											<div class="custom-control custom-checkbox mb-0">
												<input type="checkbox" class="custom-control-input" id="customCheckAllrow">
												<label class="custom-control-label" for="customCheckAllrow">Import E-Stamp - As = TTB</label>
											</div>
										</td>
										
										<td class="text-center pb-2 border-bottom-0">&nbsp;</td>	
										<td class="text-center pl-2 pb-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
									</tr>
									<tr>
										<td class="pb-2 border-bottom-0">
											<div class="custom-control custom-checkbox mb-0">
												<input type="checkbox" class="custom-control-input" id="customCheckAllrow">
												<label class="custom-control-label" for="customCheckAllrow">Import E-Stamp - As = TTB</label>
											</div>
										</td>
										
										<td class="text-center pb-2 border-bottom-0">&nbsp;</td>	
										<td class="text-center pl-2 pb-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-2 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
									</tr>

									
									<tr class="bg-light p-0 m1-3">
										<td class="pt-1 pb-1 pl-4" colspan="6">
											<strong class="text-black ml-3">Report</strong>
										</td>
										
									</tr>
								</tbody>
						</table>
			</div>
			 
			   
			</div>
			<div class="modal-footer pt-3 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-outline-dark rounded-05 col-3 mr-4" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3 ml-4" data-dismiss="modal" data-toggle="modal" data-target="#payModalsuccess">Submit</button>
			</div>
		</div>
	</div>
</div>
<!-- /Modal -->
    

    <?php include("incs/js.html") ?>
	
	 <link rel="stylesheet" href="css/vendor/bootstrap-tagsinput.css" />
	 <script src="js/vendor/datatables.min.js"></script>
    <script src="js/vendor/bootstrap-tagsinput.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
} );
	</script>
	<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(6)").addClass('active');
$(".sub-menu .list-unstyled:nth-child(3)>li:nth-child(5)").addClass('active');
</script>
</body>

</html>