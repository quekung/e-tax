<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Stamp</h1>
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Stamp</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Manual key-in</li>
							</ol>
						</nav>

                    </div>


					<div class="card">
						
						<div class="card-body">
						<div class="card-title text-medium text-center">แบบขอเสียอากรแสตมป์เป็นตัวเงินสำหรับตราสารอิเล็กทรอนิกส์ อ.ส.9</div>
							<form method="post" class="form-keyin">
								<!-- box -->
								<div class="box mb-2">
									<h2 class="h-bar h6">ลักษณะตราสารอิเล็กทรอนิกส์</h2>
									<div class="box-body">
										<div class="d-flex flex-wrap">
											<div class="form-group col-sm-6 mb-3">
												<label>ลักษณะแห่งตราสาร<span class="text-danger">*</span></label>
												<select class="form-control select2-normal select-action-chd" data-width="100%" data-placeholder="ระบุลักษณะแห่งตราสาร">
													<option></option>
													<option value="1">ตราสาร 4 จ้างทำของ</option>
													<option value="2">ตราสาร 5 กู้ยืมเงิน</option>
													<option value="3">ตราสาร 7 ใบมอบอำนาจ</option>
													<option value="4">ตราสาร 8 ใบมอบฉันทะ</option>
													<option value="5">ตราสาร 17 ค้ำประกัน</option>
												</select>
											</div>


											<div class="form-group col-sm-6 mb-3">
												<label>ผู้ขอเสียอากรแสตมป์ ในฐานะ<span class="text-danger">*</span></label>
												<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกผู้ขอเสียอากรแสตมป์ ในฐานะ">
													<option></option>
													<option>option 1</option>
													<option>option 2</option>
												</select>
											</div>
										</div>
					
									</div>
									
								</div>
								<!-- /box -->
																
								<!-- box -->
								<div class="box mb-2">
									<h2 class="h-bar h6">รายละเอียดเกี่ยวกับสัญญา</h2>
									<div class="box-body">
										<div class="d-flex flex-wrap">
											<div class="form-group col-12 mb-3">
												<label>หมายเลขอ้างอิงตราสารอิเล็กทรอนิกส์<span class="text-danger">*</span></label>
												<input class="form-control rounded-05" placeholder="ระบุหมายเลขอ้างอิงตราสารอิเล็กทรอนิกส์">
											</div>
											
											<div class="form-group col-sm-6 mb-3">
												<label>สัญญาเลขที่</label>
												<input class="form-control rounded-05" placeholder="ระบุสัญญาเลขที่">
											</div>
											
											<div class="form-group col-sm-6 mb-3">
												<label>วันที่ทำสัญญา (ลงวันที่)</label>
												<div class="input-group date">
													<span class="input-group-text input-group-append input-group-addon">
														<i class="simple-icon-calendar"></i>
													</span>
													<input type="text" class="form-control datepicker" placeholder="วัน/เดือน/ปี">
												</div>
											</div>
											
											<div class="form-group col-sm-6 mb-3">
												<label>วัน เดือน ปี ที่เริ่มสัญญา</label>
												<div class="input-group date">
													<span class="input-group-text input-group-append input-group-addon">
														<i class="simple-icon-calendar"></i>
													</span>
													<input type="text" class="form-control" placeholder="วัน/เดือน/ปี">
												</div>
											</div>
											
											<div class="form-group col-sm-6 mb-3">
												<label>วัน เดือน ปี ที่สิ้นสุดสัญญา</label>
												<div class="input-group date">
													<span class="input-group-text input-group-append input-group-addon">
														<i class="simple-icon-calendar"></i>
													</span>
													<input type="text" class="form-control" placeholder="วัน/เดือน/ปี">
												</div>
											</div>
											
											<!-- zone type keyin -->
											<div class="group-action">
												<!-- ตราสาร 4 จ้างทำของ -->
												<div class="g-action1">
													
													<div class="d-flex flex-wrap">
														<div class="form-group col-sm-9 mb-3">
															<label>งานที่รับจ้าง<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุงานที่รับจ้าง">
														</div>
														
														<div class="form-group col-sm-3 mb-3">
															<label>จำนวนของงาน<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="จำนวนของงาน">
														</div>

														<div class="form-group col-sm-6 mb-3">
															<label>มูลค่าของตราสาร (ไม่รวมภาษีมูลค่าเพิ่ม)<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุมูลค่าของตราสาร (บาท)">
														</div>

														<div class="form-group col-sm-6 mb-3">
															<label>จำนวนเงินค้ำประกันตามสัญญา</label>
															<input class="form-control rounded-05" placeholder="ระบุผู้จำนวนเงินค้ำประกันตามสัญญา (บาท)">
														</div>

													</div>
													
												</div>
												<!-- ตราสาร 5 กู้ยืมเงิน -->
												<div class="g-action2">
			
													<div class="d-flex flex-wrap">
														<div class="form-group col-sm-8 mb-3">
															<label>หลักประกัน</label>
															<input class="form-control rounded-05" placeholder="ระบุหลักประกัน">
														</div>
														
														<div class="form-group col-sm-4 mb-3">
															<label>มูลค่าตราสาร (ไม่รวมภาษีมุลค่าเพิ่ม)<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุมูลค่าตราสาร">
														</div>

														<div class="form-group col-sm-2 mb-3">
															<label>คำนำหน้าชื่อ<span class="text-danger">*</span></label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือก">
																<option></option>
																<option>นาย</option>
																<option>นาง</option>
																<option>นางสาว</option>
															</select>
														</div>

														<div class="form-group col-sm-3 mb-3">
															<label>ชื่อ<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุชื่อ">
														</div>
														
														<div class="form-group col-sm-3 mb-3">
															<label>นามสกุล<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุนามสกุล">
														</div>
														
														<div class="col-12">
															<div class=" d-flex justify-content-between align-items-center bg-light p-2">
																<label class="text-medium m-0">จำนวนผู้ค้ำประกัน :   <span class="pl-3 d-inline-block pr-3"> 0</span>     คน</label>
																<a href="manual-keyin.php" class="btn btn-success text-small rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> เพิ่มผู้ค้ำประกัน</a>
															</div>
														</div>

													</div>
												</div>
												
												<!-- ตราสาร 7 ใบมอบอำนาจ -->
												<div class="g-action3">
													<div class="d-flex flex-wrap">
														
														<div class="form-group col-12 mb-3">
															<label>รายละเอียดการมอบอำนาจ<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุรายละเอียดการมอบอำนาจ">
														</div>

														<div class="form-group col-12 mb-3">
															<label>เงื่อนไขการมอบอำนาจ<span class="text-danger">*</span></label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกเงื่อนไขการมอบอำนาจ">
																<option></option>
																<option>เงื่อนไข1</option>
																<option>เงื่อนไข2</option>
																<option>เงื่อนไข3ว</option>
															</select>
														</div>
													</div>
												</div>
												
												<!-- ตราสาร 8 ใบมอบฉันทะ -->
												<div class="g-action4">
													<div class="d-flex flex-wrap">
														
														<div class="form-group col-12 mb-3">
															<label>เงื่อนไขการมอบฉันทะ<span class="text-danger">*</span></label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกเงื่อนไขการมอบฉันทะ">
																<option></option>
																<option>เงื่อนไข1</option>
																<option>เงื่อนไข2</option>
																<option>เงื่อนไข3</option>
															</select>
														</div>
														
														
														<div class="form-group col-12 mb-3">
															<label>รายละเอียดการฉันทะ<span class="text-danger">*</span></label>
															<textarea class="form-control rounded-05" placeholder="ระบุรายละเอียดการฉันทะ" rows="2">
															</textarea>
														</div>
														
														<div class="col-12 mb-3">
															<div class="custom-switch custom-switch-primary mb-2 custom-switch-small d-flex align-items-center">
																<input class="custom-switch-input" id="switchS" type="checkbox">
																<label class="custom-switch-btn" for="switchS"></label> <span class="ml-2">กรณี Custodian</span>
															</div>
														</div>
														
														<div class="form-group col-12 mb-3">
															<label>มอบฉันทะสำหรับการประชุมบริษัท<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุเงื่อนไขการมอบฉันทะ">
														</div>
														
														<div class="form-group col-sm-4 mb-3">
															<label>ประชุมครั้งที่</label>
															<input class="form-control rounded-05" placeholder="ระบุประชุมครั้งที่">
														</div>
														
														<div class="form-group col-sm-4 mb-3">
															<label>วันที่ประชุม<span class="text-danger">*</span></label>
															<div class="input-group date">
																<span class="input-group-text input-group-append input-group-addon">
																	<i class="simple-icon-calendar"></i>
																</span>
																<input class="form-control datepicker" placeholder="ระบุวัน/เดือน/ปี">
															</div>
														</div>
														
														<div class="form-group col-sm-4 mb-3">
															<label>เวลาที่ประชุม</label>
															<input class="form-control rounded-05" placeholder="ระบุเวลาที่ประชุม">
														</div>
														
														
														<div class="form-group col-12 mb-3">
															<label>สถานที่ประชุม</label>
															<input class="form-control rounded-05" placeholder="ระบุสถานที่ประชุม">
														</div>
													</div>
												</div>
												
												<!-- ตราสาร 17 ค้ำประกัน -->
												<div class="g-action5">
													<div class="d-flex flex-wrap">
														<div class="form-group col-12 mb-3">
															<label>จำนวนเงินค้ำประกัน</label>
															<input class="form-control rounded-05" placeholder="ระบุจำนวนเงินค้ำประกัน">
														</div>
														
														<div class="form-group col-12 mb-3">
															<label>เงื่อนไขการค้ำประกัน<span class="text-danger">*</span></label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกเงื่อนไขการค้ำประกัน">
																<option></option>
																<option>เงื่อนไข1</option>
																<option>เงื่อนไข2</option>
																<option>เงื่อนไข3</option>
															</select>
														</div>

														
														<div class="form-group col-12 mb-3">
															<label>เลขประจำตัวผู้เสียภาษีอากรของเจ้าหนี้ฝผู้ว่าจ้าง</label>
															<input class="form-control rounded-05" placeholder="ระบุเลขประจำตัวผู้เสียภาษีอากรของเจ้าหนี้ / ผู้ว่าจ้าง">
														</div>


														
														<div class="form-group col-sm-2 mb-3">
															<label>คำนำหน้าชื่อ<span class="text-danger">*</span></label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือก">
																<option></option>
																<option>นาย</option>
																<option>นาง</option>
																<option>นางสาว</option>
															</select>
														</div>

														<div class="form-group col-sm-3 mb-3">
															<label>ชื่อ<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุชื่อ">
														</div>
														
														<div class="form-group col-sm-3 mb-3">
															<label>นามสกุล<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุนามสกุล">
														</div>
														
														<div class="col-12">
															<div class=" d-flex justify-content-between align-items-center bg-light p-2">
																<label class="text-medium m-0">จำนวนเจ้าหนี้ / ผู้ว่าจ้าง :   <span class="pl-3 d-inline-block pr-3"> 0</span>     คน</label>
																<a href="manual-keyin.php" class="btn btn-success text-small rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> เพิ่มเจ้าหนี้ / ผู้ว่าจ้าง</a>
															</div>
														</div>

													</div>
												</div>
											</div>
											<!-- /zone type keyin -->
											
											
											<div class="form-group col-12 mb-3 d-flex flex-wrap align-items-center">
												<label class="mb-0">ประเภทการยื่น</label>
												<div class="d-inline-block ml-4 col-12 col-sm-auto">
													<div class="custom-control custom-radio">
														<input type="radio" id="typeSubmit" name="typeSubmit" class="custom-control-input" onClick="$('#tax-append').hide();">
														<label class="custom-control-label" for="typeSubmit">ยื่นปกติ</label>
													</div>
												</div>
												
												<div class="d-inline-block ml-4 col-12 col-sm-auto">
													<div class="custom-control custom-radio">
														<input type="radio" id="typeSubmit-append" name="typeSubmit" class="custom-control-input" onClick="$('#tax-append').show();">
														<label class="custom-control-label" for="typeSubmit-append">ยื่นเพิ่มเติม</label>
													</div>
												</div>
											</div>
											
											<div class="col-12 mb-3"><div class="border-bottom border-light"></div></div>
											<!-- check -->
											<div id="tax-append" class="w-100" style="display: none">
												<div class="d-flex flex-wrap mb-4">
													<div class="form-group col-12 mb-3">
														<label>หมายเลขอ้างอิงตราสารอิเล็กทรอนิกส์<span class="text-danger">*</span></label>
														<input class="form-control rounded-05" placeholder="ระบุหมายเลขอ้างอิงตราสารอิเล็กทรอนิกส์">
													</div>

													<div class="form-group col-sm-6 mb-3">
														<label>สัญญาเลขที่</label>
														<input class="form-control rounded-05" placeholder="ระบุสัญญาเลขที่ฉบับเดิม">
													</div>

													<div class="form-group col-sm-6 mb-3">
														<label>วัน เดือน ปี ที่เริ่มสัญญา</label>
														<div class="input-group date">
														<span class="input-group-text input-group-append input-group-addon">
															<i class="simple-icon-calendar"></i>
														</span>
														<input type="text" class="form-control" placeholder="วัน/เดือน/ปี">
													</div>
													</div>

												</div>
											</div>
											<!-- /check -->
											
											<div class="form-group col-sm-4 mb-3">
												<label>ค่าอากรสแตมป์</label>
												<input class="form-control rounded-05" placeholder="ระบุค่าอากรสแตมป์">
											</div>
											
											<div class="form-group col-sm-4 mb-3">
												<label>เงินเพิ่ม</label>
												<input class="form-control rounded-05" placeholder="ระบุเงินเพิ่ม">
											</div>
											
											<div class="form-group col-sm-4 mb-3">
												<label>เบี้ยค่าปรับ</label>
												<input class="form-control rounded-05" placeholder="ระบุเบี้ยค่าปรับ">
											</div>
											


											
										</div>
										
										
					
									</div>
									
								</div>
								<!-- /box -->
								
								<!-- box -->
								<div class="box mb-2">
									<h2 class="h-bar h6">ข้อมูลคู่สัญญา (ผู้ให้กู้)</h2>
									<div class="box-body">
										<div class="d-flex flex-wrap">
										
											<div class="form-group col-12 mb-3 d-flex flex-wrap align-items-center">
												<div class="d-inline-block mr-4 p-0 col-5 col-md-3">
													<div class="custom-control custom-radio">
														<input type="radio" id="typeContract1" name="typeContract" class="custom-control-input" onClick="$('#t-contract1').show(); $('#t-contract2').hide();">
														<label class="custom-control-label" for="typeContract1">กรณีเป็นบุคคลธรรมดา</label>
													</div>
												</div>
												
												<div class="d-inline-block mr-4 p-0 col-5 col-md-3">
													<div class="custom-control custom-radio">
														<input type="radio" id="typeContract2" name="typeContract" class="custom-control-input" onClick="$('#t-contract1').hide(); $('#t-contract2').show();">
														<label class="custom-control-label" for="typeContract2">กรณีเป็นนิติบุคคล</label>
													</div>
												</div>
											</div>
											
											<div class="col-12 mb-3"><div class="border-bottom border-light"></div></div>
											
											<div class="form-group col-12 mb-3 d-flex flex-wrap align-items-center">
												<div class="d-inline-block mr-4 p-0 col-5 col-md-3">
													<div class="custom-control custom-radio">
														<input type="radio" id="liveTH" name="ContractConutry" class="custom-control-input" onClick="$('.idTax').show();">
														<label class="custom-control-label" for="liveTH">อยู่ในประเทศไทย</label>
													</div>
												</div>
												
												<div class="d-inline-block mr-4 p-0 col-5 col-md-3">
													<div class="custom-control custom-radio">
														<input type="radio" id="liveINTER" name="ContractConutry" class="custom-control-input" onClick="$('.idTax').hide();">
														<label class="custom-control-label" for="liveINTER">อยู่ในต่างประเทศ</label>
													</div>
												</div>
											</div>
											
											<div class="col-12 mb-5"><div class="border-bottom border-light"></div></div>
											<div class="radio-action w-100">
												<!-- กรณีเป็นบุคคลธรรมดา -->
												<div id="t-contract1">
												<div class="g-action1 d-flex flex-wrap">
													<div class="idTax form-group col-sm mb-3">
														<label>เลขประจำตัวผู้เสียอากร<span class="text-danger">*</span></label>
														<input class="form-control rounded-05" placeholder="ระบุเลขประจำตัวผู้เสียอากร">
													</div>

													<div class="form-group col-sm-auto mb-3">
														<label>คำนำหน้าชื่อ<span class="text-danger">*</span></label>
														<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือก">
															<option></option>
															<option>นาย</option>
															<option>นาง</option>
															<option>นางสาว</option>
														</select>
													</div>

													<div class="form-group col-sm mb-3">
														<label>ชื่อ<span class="text-danger">*</span></label>
														<input class="form-control rounded-05" placeholder="ระบุชื่อ">
													</div>

													<div class="form-group col-sm mb-3">
														<label>นามสกุล<span class="text-danger">*</span></label>
														<input class="form-control rounded-05" placeholder="ระบุนามสกุล">
													</div>
												</div>
												</div>
												
												<!-- กรณีเป็นนิติบุคคล -->
												<div id="t-contract2">
												<div class="g-action2 d-flex flex-wrap">
													<div class="idTax form-group col-sm-3 mb-3">
														<label>เลขประจำตัวผู้เสียอากร<span class="text-danger">*</span></label>
														<input class="form-control rounded-05" placeholder="ระบุเลขประจำตัวผู้เสียอากร">
													</div>

													<div class="form-group col-sm-auto mb-3">
														<label>คำนำหน้าชื่อ<span class="text-danger">*</span></label>
														<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือก">
															<option></option>
															<option>นาย</option>
															<option>นาง</option>
															<option>นางสาว</option>
														</select>
													</div>

													<div class="form-group col-sm mb-3">
														<label>ชื่อ<span class="text-danger">*</span></label>
														<input class="form-control rounded-05" placeholder="ระบุชื่อ">
													</div>
													
													<div class="clearfix col-12" style="height: 1px">&nbsp;</div>
													
													<div class="form-group col-sm-6 mb-3">
														<label>สาขา<span class="text-danger">*</span></label>
														<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกสาขา">
															<option></option>
															<option>option1</option>
															<option>option2</option>
															<option>option3</option>
														</select>
													</div>
													
													<div class="form-group col-sm-6 mb-3">
														<label>ประเภทสาขา<span class="text-danger">*</span></label>
														<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกประเภทสาขา">
															<option></option>
															<option>option1</option>
															<option>option2</option>
															<option>option3</option>
														</select>
													</div>
												</div>
												</div>
												
												
											</div>
												
											<div class="col-12 mb-2">&nbsp;</div>	
											
											<div class="form-group col-sm-4 mb-3">
												<label>ที่อยู่: อาคาร</label>
												<input class="form-control rounded-05" placeholder="ระบุอาคาร">
											</div>
											<div class="form-group col-sm-4 mb-3">
												<label>ห้องเลขที่</label>
												<input class="form-control rounded-05" placeholder="ระบุห้องเลขที่">
											</div>
											<div class="form-group col-sm-4 mb-3">
												<label>ห้องเลขที่</label>
												<input class="form-control rounded-05" placeholder="ระบุห้องเลขที่">
											</div>
											
											<div class="form-group col-sm-4 mb-3">
												<label>หมู่บ้าน</label>
												<input class="form-control rounded-05" placeholder="ระบุหมู่บ้าน">
											</div>
											<div class="form-group col-sm-4 mb-3">
												<label>เลขที่<span class="text-danger">*</span></label>
												<input class="form-control rounded-05" placeholder="ระบุเลขที่">
											</div>
											<div class="form-group col-sm-4 mb-3">
												<label>หมู่ที่</label>
												<input class="form-control rounded-05" placeholder="ระบุหมู่ที่">
											</div>
											
											<div class="form-group col-sm-4 mb-3">
												<label>จังหวัด<span class="text-danger">*</span></label>
												<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกจังหวัด">
													<option></option>
													<option>option1</option>
													<option>option2</option>
													<option>option3</option>
												</select>
											</div>
											<div class="form-group col-sm-4 mb-3">
												<label>อำเภอ/เขต<span class="text-danger">*</span></label>
												<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกอำเภอ/เขต">
													<option></option>
													<option>option1</option>
													<option>option2</option>
													<option>option3</option>
												</select>
											</div>
											<div class="form-group col-sm-4 mb-3">
												<label>ตำบล/แขวง<span class="text-danger">*</span></label>
												<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกตำบล/แขวง">
													<option></option>
													<option>option1</option>
													<option>option2</option>
													<option>option3</option>
												</select>
											</div>
											<div class="form-group col-sm-4 mb-3">
												<label>รหัสไปรษณีย์<span class="text-danger">*</span></label>
												<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกรหัสไปรษณีย์">
													<option></option>
													<option>option1</option>
													<option>option2</option>
													<option>option3</option>
												</select>
											</div>
											

											
											<div class="main-ctrl-add col-12 mb-3">
												<div class=" d-flex justify-content-between align-items-center bg-light p-2">
													<label class="text-medium m-0">จำนวนคู่สัญญาร่วม:   <span class="pl-3 d-inline-block pr-3"> 0</span>     คน</label>
													<a href="javascript:;" onClick="$(this).parents('.main-ctrl-add').next('.contractParty').slideToggle();" class="btn btn-success text-small rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> เพิ่มคู่สัญญา</a>
												</div>
											</div>
											
											<!-- คู่สัญญา -->
											<div class="contractParty">
												<div class="d-flex flex-wrap mb-3">

												<h4 class="text-small font-weight-bold mb-3 mt-3 col-12">ลำดับที่  1</h4>

												<div class="form-group col-12 mb-3 d-flex flex-wrap align-items-center">
													<div class="d-inline-block mr-4 p-0 col-5 col-md-3">
														<div class="custom-control custom-radio">
															<input type="radio" id="typeContractP1" name="typeContractParty" class="custom-control-input" onClick="$('#t-contractP1').show(); $('#t-contractP2').hide();" checked>
															<label class="custom-control-label" for="typeContractP1">กรณีเป็นบุคคลธรรมดา</label>
														</div>
													</div>

													<div class="d-inline-block mr-4 p-0 col-5 col-md-3">
														<div class="custom-control custom-radio">
															<input type="radio" id="typeContractP2" name="typeContractParty" class="custom-control-input" onClick="$('#t-contractP1').hide(); $('#t-contractP2').show();">
															<label class="custom-control-label" for="typeContractP2">กรณีเป็นนิติบุคคล</label>
														</div>
													</div>
												</div>

												<div class="col-12 mb-3"><div class="border-bottom border-light"></div></div>

												<div class="form-group col-12 mb-3 d-flex flex-wrap align-items-center">
													<div class="d-inline-block mr-4 p-0 col-5 col-md-3">
														<div class="custom-control custom-radio">
															<input type="radio" id="PliveTH" name="ContractPConutry" class="custom-control-input" onClick="$('.idTaxP').show();" checked>
															<label class="custom-control-label" for="PliveTH">อยู่ในประเทศไทย</label>
														</div>
													</div>

													<div class="d-inline-block mr-4 p-0 col-5 col-md-3">
														<div class="custom-control custom-radio">
															<input type="radio" id="PliveINTER" name="ContractPConutry" class="custom-control-input" onClick="$('.idTaxP').hide();">
															<label class="custom-control-label" for="PliveINTER">อยู่ในต่างประเทศ</label>
														</div>
													</div>
												</div>

												<div class="col-12 mb-5"><div class="border-bottom border-light"></div></div>
												<div class="radio-action w-100">
													<!-- กรณีเป็นบุคคลธรรมดา -->
													<div id="t-contractP1">
													<div class="g-action1 d-flex flex-wrap">
														<div class="idTaxP form-group col-sm mb-3">
															<label>เลขประจำตัวผู้เสียอากร<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุเลขประจำตัวผู้เสียอากร">
														</div>

														<div class="form-group col-sm-auto mb-3">
															<label>คำนำหน้าชื่อ<span class="text-danger">*</span></label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือก">
																<option></option>
																<option>นาย</option>
																<option>นาง</option>
																<option>นางสาว</option>
															</select>
														</div>

														<div class="form-group col-sm mb-3">
															<label>ชื่อ<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุชื่อ">
														</div>

														<div class="form-group col-sm mb-3">
															<label>นามสกุล<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุนามสกุล">
														</div>
													</div>
													</div>

													<!-- กรณีเป็นนิติบุคคล -->
													<div id="t-contractP2">
													<div class="g-action2 d-flex flex-wrap">
														<div class="idTaxP form-group col-sm-3 mb-3">
															<label>เลขประจำตัวผู้เสียอากร<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุเลขประจำตัวผู้เสียอากร">
														</div>

														<div class="form-group col-sm-auto mb-3">
															<label>คำนำหน้าชื่อ<span class="text-danger">*</span></label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือก">
																<option></option>
																<option>นาย</option>
																<option>นาง</option>
																<option>นางสาว</option>
															</select>
														</div>

														<div class="form-group col-sm mb-3">
															<label>ชื่อ<span class="text-danger">*</span></label>
															<input class="form-control rounded-05" placeholder="ระบุชื่อ">
														</div>

														<div class="clearfix col-12" style="height: 1px">&nbsp;</div>

														<div class="form-group col-sm-6 mb-3">
															<label>สาขา<span class="text-danger">*</span></label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกสาขา">
																<option></option>
																<option>option1</option>
																<option>option2</option>
																<option>option3</option>
															</select>
														</div>

														<div class="form-group col-sm-6 mb-3">
															<label>ประเภทสาขา<span class="text-danger">*</span></label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="เลือกประเภทสาขา">
																<option></option>
																<option>option1</option>
																<option>option2</option>
																<option>option3</option>
															</select>
														</div>
													</div>
													</div>


												</div>
												<!-- /คู่สัญญา -->
												
												</div>
												</div>
												
												<div class="col-12 ctrl-btn d-flex justify-content-end">
													<a href="manual-keyin.php" class="btn btn-outline-primary btn-lg rounded-05">ยกเลิก</a> 
													<a href="javascript:;" data-toggle="modal" data-target="#Modalsuccess" class="btn btn-primary ml-3 btn-lg rounded-05">ยืนยัน</a>
												</div>




											
											</div>
									</div>							
								</div>
								<!-- /box -->
								
							</form>

						</div>
					</div>
					
                </div>
            </div>
        </div>

    </main>

    <?php include("incs/popup.html") ?>

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
	
	function callToggleNum() {
		$('.select-action-chd').on('change', function() {

			if ($(this)[0].selectedIndex == 1) { 
				$('.group-action').children().hide();
				$('.group-action').children('.g-action1').show();
				
			} else if ($(this)[0].selectedIndex == 2)  {
				$('.group-action').children().hide();
				$('.group-action').children('.g-action2').show();
			} else if ($(this)[0].selectedIndex == 3)  {
				$('.group-action').children().hide();
				$('.group-action').children('.g-action3').show();
			} else if ($(this)[0].selectedIndex == 4)  {
				$('.group-action').children().hide();
				$('.group-action').children('.g-action4').show();
			} else if ($(this)[0].selectedIndex == 5)  {
				$('.group-action').children().hide();
				$('.group-action').children('.g-action5').show();
			}
		});
	}
	callToggleNum();

} );
	</script>
</body>

</html>